import Vue from 'vue'
import App from './App.vue';
import router from "./router";
import flexboxgrid from "flexboxgrid";
import VModal from 'vue-js-modal'

new Vue({
    router,
    flexboxgrid,
    VModal,
    render: h => h(App),
}).$mount('#app')